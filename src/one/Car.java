package one;

public class Car extends Car_registration {
	public  Car ()
	{
		super();
		System.out.println("Car 's Constructor with parameter.");
	}
	
	// overloading
	public  Car (String name)
	{
		super(name);
		System.out.println("Car 's Default Constructor.");
	}

}
